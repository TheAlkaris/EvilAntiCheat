# EvilAntiCheat
EvilAntiCheat the Anti-EasyAntiCheat

![EvilAntiCheat](.github/EvilAntiCheat_logo.png "EvilAntiCheat")

[![CRAN](https://img.shields.io/badge/licence-GPL%20(%3E%3D3)-blue.svg)](https://github.com/Alkaris/EvilAntiCheat)

\>> [[GNU GPLv3](https://github.com/Alkaris/EvilAntiCheat/blob/master/LICENSE)] << 


A project to monitor and track the secrecy of evil antics behind [EasyAntiCheat](https://www.easy.ac/ "EasyAntiCheat") (EAC) to track what it's doing in the background that user's may or may not be aware of. Third-party Anti-Cheat software often tend to resort to invasive malware tactics by hiding what its doing by compressing its file contents using methods like [UPX](https://upx.github.io/ "Ultimate Packer for eXecutables") or [MPRESS](http://www.matcode.com/mpress.htm "executable packer for PE32/PE32+/.NET/MAC-DARWIN ") to compress executables to protect programs against reverse engineering, and reduce programs and libraries to smaller sizes, which may sometimes or always trigger false-positives in firewalls or anti-virus software.

This project is mainly focused for Linux only, but you are free to fork and adapt as your own implementation, so long as it follows the GPLv3 Licence. If you wish to contibute to this repository you can submit a Pull Request, just be sure to include some notes on what your code does.

- Track file and directory access by EAC
- Monitor for Ring-0 insertion hooks
